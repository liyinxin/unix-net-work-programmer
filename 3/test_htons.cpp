#include <iostream>
#include <netinet/in.h>
#include <arpa/inet.h>

using namespace std;
int main()
{
    char *ip = "192.168.1.111";
    struct in_addr test;
    cout<<"ip is "<<ip<<endl;
    cout<<inet_aton(ip,&test)<<endl;
    cout<<"ip is in s_addr is "<<test.s_addr<<endl;
    char *oldip;
    oldip = inet_ntoa(test);
    cout<<"test.s_addr is "<<test.s_addr<<" and it's ip is "<<oldip<<endl;
    return 0;
}

