#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>

#define LISTENQ    1000
#define SERVER_IP "127.0.0.1"

int main()
{
    int listenfd,connfd;
    struct sockaddr_in server;
    bzero(&server,sizeof(server));//一定要初始化结构体

   
    server.sin_family = AF_INET;
    server.sin_port = htons(10000);
    server.sin_addr.s_addr = htonl(INADDR_ANY);


    //创建socket套接字，获取其文件描述符
    if((listenfd = socket(AF_INET,SOCK_STREAM,0) ) < 0){
        printf("socket error code is %d\n",errno);
        exit(0);
    }
    //绑定这个文件描述符
    if( bind(listenfd,(struct sockaddr*)&server,sizeof(server)) < 0){
        printf("bind error code is %d\n",errno);
        exit(0);
    }
    //开始监听
    if(listen(listenfd,LISTENQ) < 0){
        printf("listen error code is %d\n",errno);
        exit(0);
    }
    time_t ticks;
    char buff[1000];
    for(;;){
        connfd = accept(listenfd,(struct sockaddr *)NULL,NULL);
        printf("接收到了请求信息了\n");
        ticks = time(NULL);
        snprintf(buff,sizeof(buff),"%.24s\r\n",ctime(&ticks));
        write(connfd,buff,sizeof(buff));
        close(connfd);
    }
    return 0;
}

