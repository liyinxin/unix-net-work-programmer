#include <stdio.h>
#include <unpv13e/unp.h>
#include <unpv13e/apueerror.h>
int main(int argc,char **argv)
{
    int sockfd,n;
    char recvline[MAXLINE+1];
    struct sockaddr_in servaddr;//描述IP地址的
    if(argc != 2)//运行程序的格式必须是./a.out IP地址
        err_quit("Usage: a.out <IPaddress>");
    if( (sockfd = socket(AF_INET,SOCK_STREAM,0)) <0)
        err_sys("socket error");

    //初始化servaddr
    bzero(&servaddr,sizeof(servaddr));//servaddr 是一个网际套接字的结构体
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(10000);//host to network short；端口号跟ip地址必须转换成特定的格式，端口号用host to netwrok short函数转换(htons)

    //sin_addr是struct in_addr唯一的成员，类型是uint32，它也需要进行特定的转换，用inet_pton() inet presentation to network
    /*extern int inet_pton (int __af, const char *__restrict __cp,
		      void *__restrict __buf) __THROW;
    */
    if(inet_pton(AF_INET,argv[1],&servaddr.sin_addr)< 0)
        err_quit("inet_pton error for %s",argv[1]);

    if(connect(sockfd,(struct sockaddr *)&servaddr,sizeof(servaddr)) < 0)
        err_sys("connect error");

    while((n = read(sockfd,recvline,MAXLINE)) > 0){
        recvline[n] = 0;
        if(fputs(recvline,stdout) == EOF)
            err_sys("fputs error");
    }

    if(n < 0)
        err_sys("read error");
    exit(0);
}

