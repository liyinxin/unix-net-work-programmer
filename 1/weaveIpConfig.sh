#!/bin/bash
weave attach 192.168.61.2/24 prod-oai-hss
weave attach 192.168.61.3/24 prod-oai-mme
weave attach 192.168.61.4/24 prod-oai-spgwc
weave attach 192.168.61.5/24 prod-oai-spgwu-tiny
